# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/core/x86_64/libarchive/	
## Maintainer : Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
## Maintainer : Dan McGee <dan@archlinux.org>
#--------------------------------------------------------------------------------------

pkgname=libarchive
pkgver=3.3.3
pkgrel=3
arch=('x86_64')
license=('BSD')
_website="http://libarchive.org/"
pkgdesc="Multi-format archive and compression library"
url="https://github.com/libarchive/libarchive"
source=("$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz"
    'filter-xz-enable-threads.patch')

provides=(
    'libarchive.so')

depends=(
    'acl'
    'attr'
    'bzip2'
    'expat'
    'lz4'
    'libressl'
    'xz'
    'zlib'
    'zstd')

#--------------------------------------------------------------------------------------
prepare() {
    cd "$pkgname-$pkgver"

    ## Enable xz multithreaded compression by default
    patch -Np1 -i ../filter-xz-enable-threads.patch

    autoreconf -fiv
}

build() {
    cd "$pkgname-$pkgver"

    ./configure                 \
        --prefix=/usr           \
        --enable-silent-rules   \
        --disable-static        \
        --without-xml2          \
        --without-nettle
    make
}

check() {
    cd "$pkgname-$pkgver"

    make check
}

package() {
    cd "$pkgname-$pkgver"

    make DESTDIR="$pkgdir" install

    install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}

#--------------------------------------------------------------------------------------
sha512sums=('9d12b47d6976efa9f98e62c25d8b85fd745d4e9ca7b7e6d36bfe095dfe5c4db017d4e785d110f3758f5938dad6f1a1b009267fd7e82cb7212e93e1aea237bab7'
            'b176a5565ca170e4b3ce926d610cec50ea9d5d0c6d5806092d3ff1a9e7b7649f26a0cbffe0c039f5d7e85ee655aea9581f0f2dcf4b398faa219731089b70e523')
